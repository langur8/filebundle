<?php
namespace dlouhy\FileBundle\Entity;

/**
 * Entita galerie obrazku
 *
 * @author Vaclav Dlouhy <vdlouhy@atlas.cz>
 */


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Entity
 * @ORM\Table(name="file__file_galleries", indexes={ 
 * 		@ORM\Index(name="idx_search", columns={"folder"}),
 * 		@ORM\Index(name="idx_active", columns={"active"}),
 * 		@ORM\Index(name="idx_deleted", columns={"deleted"}),
 * 		@ORM\Index(name="idx_modified", columns={"modified"})
 * 		})
 * @ORM\HasLifecycleCallbacks()
 *
 */
class FileGallery extends EntityAbstract
{

	/**
	 * @var int
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $modified;

	/**
	 * @var bool
	 * @ORM\Column(type="boolean")
	 */
	protected $active = true;

	/**
	 * @var bool
	 * @ORM\Column(type="boolean")
	 */
	protected $deleted = false;

	/**
	 * @var string
	 * @ORM\Column(type="string", length=90, nullable=true)
	 */
	protected $name;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $folder;

	/**
	 * @var \Doctrine\Common\Collections\ArrayCollection
	 * @ORM\OneToMany(targetEntity="File", mappedBy="fileGallery", orphanRemoval=true, cascade={"persist", "remove"})
	 * @ORM\OrderBy({"position" = "DESC"})
	 */
	protected $files;

	/**
	 * @var File
	 * @ORM\OneToOne(targetEntity="File")
	 * @ORM\JoinColumn(name="main_file_id", referencedColumnName="id", onDelete="SET NULL")
	 */
	protected $main;


	/**
	 * @ORM\PreUpdate
	 */
	public function preUpdate()
	{
		$this->modified = new \DateTime;
	}


	/**
	 * @ORM\PrePersist
	 */
	public function prePersist()
	{
		$this->created = new \DateTime;
	}


	/**
	 * Sets a folder
	 */
	public function setUniqueFolder()
	{
		$this->setFolder(sha1(uniqid(mt_rand(), true)));
	}

	/**
	 * Vybere prvni soubor jako hlavni, pokud neni nstaven
	 */
	public function setDefaults($files = null)
	{
		
		if(!$files instanceof \Doctrine\Common\Collections\Collection) {
			$files = $this->files;
		}
		
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('deleted', false));
		
		$first = $files->matching($criteria)->first();
		
		if ($this->getMain() === null) {					
			$this->setMain($first === false ? null : $first);			
		}
	}


	/**
	 * gallerie bez main file
	 * @return array
	 */
	public function getNonMain()
	{
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('deleted', false));		
		
		$gallery = $this->files->matching($criteria)->toArray();
		if ($this->getMain() !== null) {
			foreach ($gallery as $key => $file) {
				if ($file->getId() === $this->getMain()->getId()) {
					unset($gallery[$key]);
				}
			}
		}
		return $gallery;
	}

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPresentFiles()
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('deleted', false));

        return $this->files->matching($criteria);
    }


//GENERATED

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return FileGallery
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return FileGallery
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return FileGallery
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return FileGallery
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return FileGallery
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set folder
     *
     * @param string $folder
     * @return FileGallery
     */
    public function setFolder($folder)
    {
        $this->folder = $folder;

        return $this;
    }

    /**
     * Get folder
     *
     * @return string 
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * Add files
     *
     * @param \dlouhy\FileBundle\Entity\File $files
     * @return FileGallery
     */
    public function addFile(\dlouhy\FileBundle\Entity\File $files)
    {
        $this->files[] = $files;

        return $this;
    }

    /**
     * Remove files
     *
     * @param \dlouhy\FileBundle\Entity\File $files
     */
    public function removeFile(\dlouhy\FileBundle\Entity\File $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set main
     *
     * @param \dlouhy\FileBundle\Entity\File $main
     * @return FileGallery
     */
    public function setMain(\dlouhy\FileBundle\Entity\File $main = null)
    {
        $this->main = $main;

        return $this;
    }

    /**
     * Get main
     *
     * @return \dlouhy\FileBundle\Entity\File 
     */
    public function getMain()
    {
        return $this->main;
    }	
 
}
