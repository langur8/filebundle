<?php

namespace dlouhy\FileBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use dlouhy\FileBundle\Entity\Intervencionalist;

class FileType extends AbstractType
{


	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
				->add('translations', 'a2lix_translations', array(
					'label' => false,
					'fields' => array(
						'description' => array(
							'label' => 'Popis'
				))))
				->add('position', 'integer', array(
					'label' => 'Priorita řazení'
		));
	}


	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'dlouhy\FileBundle\Entity\File'
		));
	}


	public function getName()
	{
		return 'file';
	}

}
