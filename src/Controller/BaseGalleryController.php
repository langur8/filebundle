<?php

namespace dlouhy\FileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use dlouhy\SimpleCRUDBundle\Exception\SaveException;
use Symfony\Component\Form\FormError;

use dlouhy\FileBundle\Entity\FileGallery;
use dlouhy\FileBundle\Entity\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

abstract class BaseGalleryController extends Controller
{

	/**
	 * Nazev entity ke ktere je galerie ulozena
	 *
	 * @var string
	 */
	protected $sParentEntity;
	
	/**
	 * Nazev promenne entity sParentEntity, ke ktere je galerie ulozena
	 *
	 * @var string
	 */	
	protected $sParentProperty;
	
	
	/**
	 * Getter pro galerii k predkovi
	 *
	 * @var string
	 */	
	private $sParentGetter;
	
	/**
	 * Setter pro galerii k predkovi
	 *
	 * @var string
	 */		
	private $sParentSetter;
	
	/**
	 * Nazev editacniho formulare
	 *
	 * @var string
	 */
	protected $sForm;	
	
	/**
	 * Nazev entity balicku kratky
	 *
	 * @var string
	 */
	protected $sBundle;

	/**
	 * Nazev controlleru
	 *
	 * @var string
	 */
	protected $sController;

	/**
	 * Nazev akce
	 *
	 * @var string
	 */
	protected $sAction;

	/**
	 * Umisteni sablony s dvojteckovou notaci
	 *
	 * @var string
	 */
	protected $sTemplate;

	/**
	 * Umisteni sablony formulare s dvojteckovou notaci
	 *
	 * @var string
	 */
	protected $sFormTemplate;	
	
	/**
	 * Presmerovani
	 * 
	 * @var string
	 */
	protected $redirect;
	
	/**
	 * Entita, k niz je vazana galerie
	 */
	protected $parent;	
	
	/**
	 * instance Galerie, vetsinou FileGallery
	 * @var FileGallery
	 */
	protected $gallery;
	
	/**
	 * @var bool
	 */
	private $initialized = false;
	
	public function baseGalleryAction(Request $request, $id)
	{		
		$this->init($request, $id);
			
		if($this->redirect === null) {
			$this->redirect = $this->generateUrl($request->get('_route').'_save', array('id' => $id));
		}	
		
		$form = $this->getForm($this->gallery, $this->redirect);
		
		if($request->isXmlHttpRequest()) {
			return new JsonResponse(array(
				'replace' => true,
				'elementId' => 'gallery',
				'html' => $this->renderView($this->sFormTemplate, array(
					'parent' => $this->parent,
					'form' => $form->createView(),
					'deleteRoute' => $request->get('_route').'_delete',
					'errors' => $this->get('form.form_errors')->getArray($form)
				))), 200);			
		}
		
		
		return $this->render($this->sTemplate, array(
			'form' => $form->createView(),
			'parent' => $this->parent,
			'uploadPath' => $this->generateUrl($request->get('_route').'_upload', array('id' => $id)),
			'deleteRoute' => $request->get('_route').'_delete',
			'showPath' => $this->generateUrl($request->get('_route'), array('id' => $id))
			));

	}

	
	public function baseUploadFiles(Request $request, $id)
	{
		
		$this->init($request, $id);

		$files = $request->files->get('gallery');

		if (empty($files) || !isset($files['file']['file'])) {
			return new JsonResponse(array('msg' => 'No files submited'), 400);
		}
		
		call_user_func(array($this->parent, $this->sParentSetter), $this->gallery);
		$this->save($this->parent);		
		
		foreach ($files['file']['file'] as $file) {

			if (!$file instanceof UploadedFile) {
				return new JsonResponse(array('File not included'), 400);
			}

			$this->get('dlouhy_file.file_service')->upload($file, $this->gallery);
		}

		return new JsonResponse(array('msg' => 'OK'), 200);
	}
	
	
	public function baseSaveGalleryAction(Request $request, $id)
	{
		$this->init($request, $id);
				
		$form = $this->getForm($this->gallery, $this->redirect);
		$form->handleRequest($request);			

		$message = '';
		$returnCode = 400;		
		if ($form->isValid()) {
			try {
				$this->gallery->setDefaults();				
				$this->save($this->gallery);
				$form = $this->getForm($this->gallery, $this->redirect);
				$returnCode = 200;
			} catch(SaveException $e) {
				$this->processSaveException($form, $e);
			} 
		}
		
		$addRedirect = array();
		if($this->redirect !== null) {
			$addRedirect['redirect'] = $this->redirect;
		}

		return new JsonResponse(				
			array(
			'replace' => true,
			'html' => $this->renderView($this->sFormTemplate, array(
				'parent' => $this->parent,
				'form' => $form->createView(),
				'deleteRoute' => str_replace('_save', '_delete', $request->get('_route')),
				'errors' => $this->get('form.form_errors')->getArray($form)
			))) + $addRedirect, $returnCode);
	}		

	
	public function baseDeleteFile(Request $request, $id, $fileId)
	{

		$this->init($request, $id);

		if (!$fileId) {
			throw $this->createNotFoundException('Bad parameter id');
		}

		$repoImg = $this->getDoctrine()->getRepository('dlouhy\FileBundle\Entity\File');
		$file = $repoImg->find($fileId);

		if (!$file instanceof File) {
			throw $this->createNotFoundException('The File does not exist');
		}

		$this->get('dlouhy_file.file_service')->delete($file);
		$form = $this->getForm($this->gallery, $this->redirect);

		return new JsonResponse(array(
			'replace' => true,
			'elementId' => 'gallery',
			'html' => $this->renderView($this->sFormTemplate, array(
				'parent' => $this->parent,
				'form' => $form->createView(),
				'deleteRoute' => $request->get('_route'),
				'errors' => $this->get('form.form_errors')->getArray($form)
			))), 200);
	}		
				
	
	protected function init(Request $request, $id)
	{
		if($this->initialized === true) {
			return;
		}
		
		$matches = array();
		$controller = $request->attributes->get('_controller');
		preg_match('/(.*)\\\Controller\\\(.*)Controller::(.*)Action/', $controller, $matches);				

		if ($this->sBundle === null) {
			$this->sBundle = str_replace("\\", '', $matches[1]);
		}
		if ($this->sParentEntity === null) {
			$this->sParentEntity = $matches[1] . '\Entity\\' . $matches[2];
		}
		if ($this->sParentProperty === null) {
			$this->sParentProperty = 'FileGallery';
		}		
		if ($this->sParentGetter === null) {
			$this->sParentGetter = 'get'.$this->sParentProperty;
		}
		if ($this->sParentSetter === null) {
			$this->sParentSetter = 'set'.$this->sParentProperty;
		}		
		if ($this->sForm === null) {
			$this->sForm = 'dlouhy\FileBundle\Form\Type\FileGalleryType';
		}				
		if ($this->sController === null) {
			$this->sController = $matches[2];
		}
		if ($this->sAction === null) {
			$this->sAction = $matches[3];
		}
		if ($this->sTemplate === null) {
			$this->sTemplate = $this->sBundle . ':' . $this->sController . ':' . $this->sAction . '.html.twig';
		}
		if ($this->sFormTemplate === null) {
			$this->sFormTemplate = $this->sBundle . ':' . $this->sController . ':gallery_form.html.twig';
		}		
		
		if (!$id) {
			throw $this->createNotFoundException('Bad parameter id');
		}

		$repo = $this->getDoctrine()->getRepository($this->sParentEntity);
		$this->parent = $repo->find($id);

		if ($this->parent === false) {
			throw $this->createNotFoundException('Not found');
		}
		
		$this->gallery = call_user_func(array($this->parent, $this->sParentGetter));

		if (!$this->gallery instanceof FileGallery) {
			$this->gallery = new FileGallery;
			$this->gallery->setUniqueFolder();
			call_user_func(array($this->parent, $this->sParentSetter), $this->gallery);
		}
		
		$this->initialized = true;
	}
	
	
	protected function getForm($entity, $action, $options = array())
	{
		return $this->createForm(new $this->sForm, $entity, array('action' => $action) + $options);
	}	
	
	
	protected function save($entity)
	{
		$em = $this->getDoctrine()->getManager();
		$em->persist($entity);
		$em->flush();
	}	
	
	
	protected function processSaveException($form, $e)
	{
		foreach($e->getFormErrors() as $list) {
			foreach($list->getIterator() as $constraintViolation) {								
				$form->get($constraintViolation->getPropertyPath())->addError(new FormError($constraintViolation->getMessage()));
			}
		}				
	}	
		
}
