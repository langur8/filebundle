<?php

namespace dlouhy\FileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

//use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class FileTranslation extends EntityAbstract
{
	use ORMBehaviors\Translatable\Translation;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $description;
	
	/**
	 * Set description
	 *
	 * @param \DateTime $description
	 * @return File
	 */
	public function setDescription($description)
	{
		$this->description = $description;

		return $this;
	}


	/**
	 * Get description
	 *
	 * @return \DateTime 
	 */
	public function getDescription()
	{
		return $this->description;
	}	

}
