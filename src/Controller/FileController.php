<?php

namespace dlouhy\FileBundle\Controller;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FileController
{
	
	/**
	 * @var Registry
	 */
	private $doctrine;
	
	public function __construct(Registry $doctrine)
	{
		$this->doctrine = $doctrine;
	}
	
    public function fileAction(Request $request, $gallery, $file)
    {
		$path = pathinfo($file);
		$filename = $path['filename'];
		$suffix = $path['extension'];
		
		//todo - files cely presounout do file bundle
        $fileRepo = $this->doctrine->getRepository('dlouhy\FileBundle\Entity\File');
		$file = $fileRepo->findByNameAndGallery($filename, $suffix, $gallery);				
		
		if($file === null) {
			throw new NotFoundHttpException('File not found');
		}									
		
		$fpath = $file->getAbsolutePath();
		
		if(!file_exists($fpath)) {
			throw new NotFoundHttpException('File not found');
		}
				
		$sha1 = $request->get('sha1');		
		if($sha1 !== null && $sha1 !== sha1_file($fpath)) {
			throw new NotFoundHttpException('Checksum not match');
		}
		
		$response = new Response;		
		$response->headers->set('Content-Type', $file->getMime());
		$response->setContent(file_get_contents($fpath));

		return $response;	
    }
}
