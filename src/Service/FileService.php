<?php
namespace dlouhy\FileBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use dlouhy\FileBundle\Entity\File;
use dlouhy\FileBundle\Entity\FileGallery;

class FileService
{
	
	/**
	 * @var Registry
	 */
	private $doctrine;


	public function __construct(Registry $doctrine)
	{
		$this->doctrine = $doctrine;
	}


	private function getDoctrine()
	{
		return $this->doctrine;
	}


	public function upload(UploadedFile $file, $gallery = null)
	{
		$f = new File;
		$f->setFile($file);
		$em = $this->getDoctrine()->getManager();

		if ($gallery instanceof FileGallery) {
			$gallery->addFile($f);
			$gallery->setDefaults();
			$f->setFileGallery($gallery);
			$em->persist($gallery);
		} else {
			$em->persist($f);
		}

		$em->flush();
	}
	
	
	public function uploadToGallery(UploadedFile $file, FileGallery $gallery = null, $singleFileGallery = false)	
	{
		if(!$gallery instanceof FileGallery) {
			$gallery = new FileGallery;
			$gallery->setUniqueFolder();
		}
		
		if($singleFileGallery === true) {
			$this->deleteGalleryFiles($gallery);
		}
		
		$this->upload($file, $gallery);
		
		return $gallery;
	}


	public function delete(File $file)
	{		
		$gallery = $file->getFileGallery();
		
		$em = $this->getDoctrine()->getManager();
		$em->remove($file);
		$em->flush();			

		if ($gallery instanceof FileGallery) {
			if ($gallery->getMain() !== null && $gallery->getMain()->getId() === $file->getId()) {
				$gallery->setMain(null);				
			}
			$gallery->removeFile($file);			
			$gallery->setDefaults();			
			$em->persist($gallery);
			$em->flush();	
		}
		
		$tpath = $file->getUploadRootDir() . '/' . $file->getPath();
		if (file_exists($tpath)) {
			unlink($tpath);
		}				

	}
	
	
	public function deleteGalleryFiles(FileGallery $gallery) 
	{
		foreach($gallery->getPresentFiles()->getIterator() as $file) {
			$this->delete($file);
		}
	}

}
