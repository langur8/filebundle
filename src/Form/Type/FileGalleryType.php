<?php

namespace dlouhy\FileBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Doctrine\ORM\EntityRepository;

class FileGalleryType extends AbstractType
{

	protected $entity;
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		
		$entity = $options['data'];	
		
        $builder->add('files', 'collection', array(
            'type' => new FileType(),
            'label' => false,
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false
        ));
		
		if($entity->getId()) {
		
			$builder->add('main', 'entity', array(
				'class' => 'dlouhy\FileBundle\Entity\File',			
				'data_class' => 'dlouhy\FileBundle\Entity\File',			
				'expanded' => true,
				'multiple' => false,
				'property' => function() {return 'Hlavní soubor galerie';},
				'label' => false,
				'query_builder' => function(EntityRepository $er) use ($entity){
					return $er->createQueryBuilder('i')->where('i.fileGallery = ?1')->andWhere('i.deleted = ?2')->setParameters(array(1 => $entity, 2 => 0));
					}			
			));
			
		}
		
		if($entity->getPresentFiles()->count() > 0) {
			$builder->add('save', 'submit', array('label' => 'OK'));
		}				  
		
	}
	
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'required' => false,
			'data_class' => 'dlouhy\FileBundle\Entity\FileGallery'
		));
	}


	public function getName()
	{
		return 'file_gallery';
	}


}
